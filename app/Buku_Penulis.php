<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku_Penulis extends Model
{
    protected $table = 'Buku_Penulis';
	protected $fillable = [

		'penulis_id','buku_id'
	];

	public function Buku()
	{
		return $this->belongsTo(Buku::class);	
	}
	public function Penulis()
	{
		return $this->belongsTo(Penulis::class);	
	}
}
