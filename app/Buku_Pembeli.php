<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku_Pembeli extends Model
{
    protected $table = 'Buku_Pembeli';
	protected $fillable = [

		'pembeli_id','buku_id'

	];

	public function Buku()
	{
		return $this->belongsTo(Buku::class);	
	}
	public function Pembeli()
	{
		return $this->belongsTo(Pembeli::class);	
	}
}
