<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'Admin';
	protected $fillable = [

		'nama','notlp','email','alamat','pengguna_id'
	];

	public function Pengguna()
	{
			return $this->belongsTo(Pengguna::class);	
	}
}
