<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buku;

class Penulis extends Model
{
    protected $table = 'penulis';
	protected $fillable = [

		'nama','notlp','email'

	];

	public function Buku()
	{			
			return $this->belongsToMany(Buku::class)
			->withPivot('id')
			->withTimestamps();
	}
	public function Buku_Penulis()
	{

			return $this->hasOne(Buku_Penulis::class);
	}
}