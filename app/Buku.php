<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Penulis;

class Buku extends Model
{
    protected $table = 'Buku';
	protected $fillable = [

		'judul','kategori_id','penerbit','tanggal'

	];

	public function Kategori()
	{
		return $this->belongsTo(Kategori::class);	
	}
	
	public function Pembeli()
	{

		return $this->hasMany(Pembeli::class);
	}
	
	public function Buku_Pembeli()
	{

		return $this->hasOne(Buku_Pembeli::class);
	}
	
	public function Penulis()
	{	
		return $this->belongsToMany(Penulis::class)
		->withPivot('id')
		->withTimestamps();
	}
	
	public function Buku_Penulis()
	{

		return $this->hasOne(Buku_Penulis::class);
	}
}