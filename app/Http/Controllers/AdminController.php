<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

class AdminController extends Controller
{
     public function awal(){
    	$admin = Admin::all();
    	return view('admin.app', compact('admin'));
    }

    public function tambah(){
    	return view("admin.tambah");
    }

    public function simpan(Request $input){
    	$admin = new Admin();
    	$admin->nama = $input->nama;
    	$admin->notlp = $input->notlp;
    	$admin->email = $input->email;
    	$admin->alamat = $input->alamat;
    	$admin->save();
    	return redirect('admin');
    }

    	 public function edit($id){
    	$admin = Admin::find($id);
    	return view('admin.edit')->with(array('admin'=>$admin));
    }

    public function update($id, Request $input){
    	$admin = Admin::find($id);
    	$admin->nama = $input->nama;
    	$admin->notlp = $input->notlp;
    	$admin->email = $input->email;
    	$admin->alamat = $input->alamat;
    	$status = $admin->save();
    	return redirect('admin')->with(['status'=>$status]);
    }

      public function hapus($id){
    	$admin = Admin::find($id);
    	$admin->delete();
    	return redirect('admin');
    }
}

