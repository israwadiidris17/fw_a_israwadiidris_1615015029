<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Kategori;

class KategoriController extends Controller
{
  public function awal(){
    $kategori = Kategori::all();
    return view('kategori.app',compact('kategori'));
  }
  public function tambah(){
   return view('kategori.tambah');
 }   
 public function simpan(Request $input){
  $this->validate($input,[
    'deskripsi' => 'required',
  ]);
  $kategori = new Kategori();
  $kategori->deskripsi = $input->deskripsi;
  $status = $kategori->save();
  return redirect('kategori')->with(['status'=>$status]);
}
public function edit($id){
  $kategori = Kategori::find($id);
  return view('kategori.edit')->with(array('kategori'=>$kategori));
}
  /* public function update($id, Request $input){
        $kategori = Kategori::find($id);
        return view('buku.editkategori')->with(array('kategori'=>$kategori));
      }*/
      public function update($id, Request $input){
        $this->validate($input,[
          'deskripsi' => 'required',   
        ]);
        $kategori = Kategori::find($id);
        $kategori->deskripsi = $input->deskripsi;
        $status = $kategori->save();
        return redirect('kategori')->with(['status'=>$status]);
      }
      public function hapus($id){
        $kategori = Kategori::find($id);
        $kategori->delete();
        return redirect('kategori');
      }
    }
