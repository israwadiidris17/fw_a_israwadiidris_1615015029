<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'Pembeli';
	protected $fillable = [

		'nama','notlp','email','alamat','pengguna_id'
	];

	public function Pengguna()
	{
			return $this->belongsTo(Pengguna::class);	
	}

	public function Buku()
	{	
			return $this->belongsToMany(Buku::class);
	}
	
	public function Buku_Pembeli()
	{
			return $this->hasOne(Buku_Pembeli::class);
	}

}
