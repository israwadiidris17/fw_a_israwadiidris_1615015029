<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'Kategori';
	protected $fillable = [

		'Deskripsi'
	];

	public function Buku()
	{
		return $this->hasMany(Buku::class);
	}
}
