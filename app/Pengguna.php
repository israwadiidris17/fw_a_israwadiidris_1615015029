<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\pembeli;

class Pengguna extends Authenticatable
{
    protected $table = 'pengguna';
    protected $fillable = [
    	'username','password'
    ];
    protected $hidden = [
    	'password','remember_token'
    ];

	public function Pembeli()
	{
		return $this->hasOne(Pembeli::class);
	}

    public function Admin()
    {
        return $this->hasOne(Admin::class);
    }

}

