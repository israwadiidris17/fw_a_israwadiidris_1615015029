
@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
<div class="panel-heading">
Data Penulis
<div class="pull-right">
<a href="{{ url('tambah/penulis')}}" class="btn btn-success btn-xs">Tambah Data</a>
</div>
</div>
<div class="panel-body">
<table class="table">
<tr>
<td>Nama</td>
<td>No Telepon</td>
<td>Email</td>
<td>Alamat</td>
</tr>
@foreach($penulis as $Penulis)
<tr>
<td >{{ $Penulis->nama }}</td>
<td >{{ $Penulis->notlp}}</td>
<td >{{ $Penulis->email }}</td>
<td >{{ $Penulis->alamat}}</td>
<td >
<a href="{{url('penulis/edit/'.$Penulis->id)}}" class="btn btn-success btn-xs">Edit</a>
<a href="{{url('penulis/hapus/'.$Penulis->id)}}" class="btn btn-danger btn-xs">Hapus</a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
@endsection
