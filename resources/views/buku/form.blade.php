<div class="form-group">
<center>
@if ($errors->any())
 <div class="alert alert-danger">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
@endif
<div class="form-group">
<label class="col-sm-3">Judul</label>
<div class="col-sm-8">
{!! Form::text('judul',null,['class'=>'formcontrol','placeholder'=>"Judul"])
!!}
</div>
</div>
<div class="form-group">
<label class="col-sm-3">Penulis</label>
<div class="col-sm-8">
{!! Form::select('penulis', $author, null, ['class' => 'formcontrol'])
!!}
</div>
</div>
<div class="form-group">
<label class="col-sm-3">Kategori</label>
<div class="col-sm-8">
{!! Form::select('kategori', $categories, null, ['class' =>
'formcontrol']) !!}
</div>
</div>
<div class="form-group">
<label class="col-sm-3">Penerbit</label>
<div class="col-sm-8">
{!! Form::text('penerbit',null,['class'=>'formcontrol','placeholder'=>"Penerbit"])
!!}
</div>
</div>
<div class="form-group">
<label class="col-sm-3">Tanggal</label>
<div class="col-sm-8">
{!! Form::text('tanggal',null,['class'=>'formcontrol','placeholder'=>"Tanggal
Rilis"]) !!}
</div>
</div>
</center>
</div>
