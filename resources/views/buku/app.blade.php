@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-default">
<div class="panel-heading">
<strong>Data Buku</strong>
<div class="pull-right">
<a href="{{ url('tambah/buku')}}" class="btn btn-success btn-xs">Tambah Data</a>
</div>
<div class="penel-body">
<table class="table">
<tr>
<td> Judul </td>
<td> Kategori </td>
<td> Penerbit </td>
<td> Penulis </td>
<td> Aksi </td>
</tr>
@foreach($datac as $buku)
<tr>
<td>{{ $buku->judul }}</td>
<td>{{ $buku->kategori->deskripsi or 'kosong'}}</td>
<td>{{ $buku->penerbit }}</td>
<td>{{ $buku->penulis->first()->nama or 'kosong'}}</td>
<td>
<a href="{{url('buku/edit/'.$buku->id)}}" class="btn btn-success btn-xs">Edit</a>
<a href="{{url('buku/hapus/'.$buku->id)}}" class="btn btn-danger btn-xs">Hapus</a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
</div>
 @endsection