<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTableBukuPembeli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku_pembeli', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pembeli_id')->unsigned();
            $table->foreign('pembeli_id')
            ->references('id')
            ->on('pembeli')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('buku_id')->unsigned();
            $table->foreign('buku_id')
            ->references('id')
            ->on('buku')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_pembeli');
    }
}
