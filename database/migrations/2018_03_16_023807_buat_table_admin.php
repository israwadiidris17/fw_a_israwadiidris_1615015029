<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTableAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            //nama, notelp,
            $table->increments('id');
            $table->string('nama');
            $table->string('notelp');
            $table->string('email');
            $table->text('alamat');
            $table->integer('pengguna_id')->unsigned();
            $table->foreign('pengguna_id')
                ->references('id')
                ->on('pengguna')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
